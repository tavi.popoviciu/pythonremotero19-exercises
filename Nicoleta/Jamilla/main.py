from clock_time import countdown as ct
class Reteta:
    timp_de_pregatire=10 #Class attribute #vom presupune ca e in secunde!
    mancare_servita=False #Class attribute
    mancare_arsa=False
    mancare_negatita=False
    ok = True



    def __init__(self,ingredient1,ingredient2,ingredient3,ingredient4,ingredient5):
        self.lista_ingrediente=[]
        self.ingredient1=ingredient1
        self.ingredient2 = ingredient2
        self.ingredient3 = ingredient3
        self.ingredient4 = ingredient4
        self.ingredient5 = ingredient5


    def amestec_ingrediente(self):
        self.lista_ingrediente.append(self.ingredient1)
        self.lista_ingrediente.append(self.ingredient2)
        self.lista_ingrediente.append(self.ingredient3)
        self.lista_ingrediente.append(self.ingredient4)
        self.lista_ingrediente.append(self.ingredient5)

    def oala(self):
        print(f"In oala magica, acum se afla aceste minunate si gustoase super ingrediente :{self.lista_ingrediente}")

    def bag_la_foc(self):
        if Reteta.ok:
            self.timp_gatire_user=int(input("Dati un timp de gatire"))
            if(self.timp_gatire_user == Reteta.timp_de_pregatire):
                ct(self.timp_gatire_user)
                print("Mancarea dvs este gata!")
                Reteta.mancare_servita=True
                Reteta.ok=False
            elif self.timp_gatire_user > Reteta.timp_de_pregatire:
                    ct(self.timp_gatire_user)
                    print("Mancarea s-a ars")
                    Reteta.mancare_arsa=True
                    Reteta.ok=False
            elif self.timp_gatire_user < Reteta.timp_de_pregatire:
                    ct(self.timp_gatire_user)
                    print("Mancarea nu e suficient de gatita!")
                    Reteta.mancare_negatita=True
                    Reteta.ok=False
        else:
            print("Hai ca-i gata mancarea")
            print("Na ca ai ars mancarea, vezi ca trebuie sa te uiti la Jamilla Cousine")
            print("Atentie, mancarea este gata deja, acum ai ars-o, bă Jamilă >:( !")

    def final_mancare(self):
        if Reteta.mancare_servita :
            print("Mancarea este gata, hai la masa")
        elif Reteta.mancare_arsa:
            print("No, s-o ars mancarea, acum rabdati de foame pana fac alta mancare")
        elif Reteta.mancare_negatita:
            print("Nu pune mana ca nu egata")


zama_de_gaina=Reteta("apa","gaina","zarzavat","dragoste","bors acru")
zama_de_gaina.amestec_ingrediente()
zama_de_gaina.oala()
zama_de_gaina.bag_la_foc()
zama_de_gaina.bag_la_foc()
zama_de_gaina.final_mancare()